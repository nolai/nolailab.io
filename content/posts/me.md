---
title: "Who am I?"
description: "Swedish guy with abou 15 years of experience with product managmena, proeject mangment and now with service managment"
date: "2016-06-23"
categories: 
    - "presentation"
    - "interesting"
---

## Personal presentation

I am..
well who am I?
First thing after high school and studying in Italy became Stockholm. I stayed there over 10 years and during that time I managed to educate myself to become a high school teacher and later work as this. But as a newly-qualified teacher, there were not so many jobs during this time, so when the opportunity was there I came back to the teachers college and I became an  IT-coach with main responsibility for the learning management system Moodle. When the teachers college would be incorporated into the University of Stockholm, I got new duties on IT & Media at Stockholm University. There it was a lot of management on most applications which  IT & Media is responsible for. And it was there that I really saw that there should be  understanding even internally within a technology-oriented organization that we always should try to put user needs first. That's what I've done since then, tried to interpret these needs so that they understand  users needs. A good roadmap is essential in that. After that we moved and bought a house in Råneå and  I got a new job on the Educational Unit at Umeå University. I worked as sysadmin in Umeå but even then I always thought about the users needs when for example servers needed to reboot and stuff like that. Everything should be about users needs and the best way we can help them and to interpret them and use constant dialogue to keep improving.
At the moment at Luleå University of Technology I work as an ICT-coach, product manager and just now a project manager for the implementation of a new learning management system. A project that will be  a real big undertaking for a year and a half. 
I am a firm believer in things like flipped classroom, peer review and formative assessment with digital tools.

So to sum up I am a 37 year old swedish guy that want’s to keep improving learning in the digital age!
### Looks like?

Like this!
<figure>   
    <img src="404.jpeg"/>
    <figcaption>
        <h4>Niklas Olaisson</h4>
    </figcaption>
</figure>

